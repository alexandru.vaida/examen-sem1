<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>import</title>
</head>
<body>
 <!-- Main Content -->
 <?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "database";
  
  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  
  $sql = "SELECT * FROM database" ;
  $result = $conn->query($sql);
  

if ($result->num_rows > 0) {
 // output data of each row
 while($row = $result->fetch_assoc()) {
     ?>
<div class="container">
<div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
     <p><?php echo $row["nume"]; ?> </p>
     <p><?php echo $row["categorie"] ; ?> </p>
     <p><?php echo $row["imagine"]; ?> </p>
 </div>
 </div>
 <?php }
} ?>
</div>
</div>
</body>
</html>
